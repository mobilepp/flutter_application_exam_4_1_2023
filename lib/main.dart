import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import './aspecratio.dart';
import './weather_forcast.dart';

void main() {
  runApp(const ExampleApp());
}

// class AspectRatioExample extends StatelessWidget {
class ExampleApp extends StatelessWidget {
  const ExampleApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    double? spacew = 25;
    double? sizew = 20;
    var currentTheme = APP_THEME.DARK;

    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        // ThemeData(
        //   primarySwatch: Colors.green,
        // ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: buildAppBarWidget(),
          // AppBar(
          //   title: const Text('App Title'),
          // ),
          body: buildBodyWidget(spacew, sizew, currentTheme),//WeatherForecastPage(),
          // body: AspectRatioExample(),
      // body: Container(),
    ),
    ),
    );

  // return MaterialApp(
  //   debugShowCheckedModeBanner: false,
  //   home: Scaffold(
  //     body: Center(
  //       child: SizedBox(
  //         width: 600.0,
  //         child: AspectRatio(
  //           aspectRatio: 1.5,
  //           child: Container(
  //             color: Colors.green[200],
  //             child: const FlutterLogo(),
  //           ),
  //         ),
  //       ),
  //     ),
  //   ),
  // );
  }

}

// class AlignExample extends StatelessWidget {
//   // const AlignExample({super.key});
//   const AlignExample({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         body: Center(
//             //     child: Container(
//             //   height: 200.0,
//             //   width: 200.0,
//             //   color: Colors.green[200],
//             //   child: const Text('Align me!'),
//             // )
//             child: Container(
//             height: 200.0,
//             width: 600.0,
//             color: Colors.green[200],
//             child: Align(
//               // alignment: const Alignment(-0.75, -0.75),
//               alignment: Alignment.topRight,
//               child: Container(
//                 color: Colors.green[200],
//                 child: const Text('Align me!'),
//               ),
//             ),
//           )
//         ),
//       ),
//     );
//   }
// }
